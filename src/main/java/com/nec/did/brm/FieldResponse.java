package com.nec.did.brm;

public enum FieldResponse {
    REQUIRED,
    MESSAGES,
    INVALID_TYPE,
    DISABLED
}
