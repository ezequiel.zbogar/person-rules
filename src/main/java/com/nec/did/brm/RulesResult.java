package com.nec.did.brm;

import com.nec.did.i18n.ErrorCodes;
import com.nec.did.i18n.RuleError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class RulesResult implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    private HashMap<FieldResponse, Object> data;
    private RulesReturnValues result;

    public RulesResult() {
        this.data = new HashMap<>();
        this.result = RulesReturnValues.SUCCESS;

        data.put(FieldResponse.REQUIRED, new ArrayList<String>());
        data.put(FieldResponse.DISABLED, new ArrayList<String>());
        data.put(FieldResponse.INVALID_TYPE, new ArrayList<String>());

        data.put(FieldResponse.MESSAGES, new ArrayList<HashMap<String, Object>>());
    }
    
    public RulesResult(HashMap<FieldResponse, Object> data, RulesReturnValues result) {
        this.data = data;
        this.result = result;
    }

    public void addRequired(String field) {
        ((ArrayList) this.data.get(FieldResponse.REQUIRED) ).add(field);
    }

    public void addDisabled(String field) {
        ((ArrayList) this.data.get(FieldResponse.DISABLED) ).add(field);
    }

    public void addInvalidType(String field) {
        ((ArrayList) this.data.get(FieldResponse.INVALID_TYPE) ).add(field);
    }

    public void addMessage(String field, ErrorCodes errorCode, Object... args) {
        ArrayList<HashMap<String, Object>> fieldMessages = (ArrayList<HashMap<String, Object>>) this.data.get(FieldResponse.MESSAGES);

        Optional<HashMap<String, Object>> messageObject = fieldMessages.stream().filter(item -> item.get("name").equals(field)).findAny();

        RuleError ruleError = new RuleError(errorCode, args);

        if (messageObject.isPresent()) {
            ArrayList<RuleError> errorsList = (ArrayList<RuleError>) messageObject.get().get("errors");
            errorsList.add(ruleError);
        } else {
            HashMap<String, Object> itemMap = new HashMap<>();
            ArrayList<RuleError> errorsList = new ArrayList<>();
            errorsList.add(ruleError);
            itemMap.put("errors", errorsList);
            itemMap.put("name", field);
            fieldMessages.add(itemMap);
        }
    }

    public void setResultsMessages(ArrayList<HashMap<String, Object>> formMessages) {
        this.data.put(FieldResponse.MESSAGES, formMessages);
    }

    public void setResult(RulesReturnValues result) {
        this.result = result;
    }

    public void setData(HashMap<FieldResponse, Object> data) {
        this.data = data;
    }

    public HashMap<FieldResponse, Object> getData() {
        return data;
    }

    public RulesReturnValues getResult() {
        return result;
    }
}
