package com.nec.did.brm;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RulesReturnValues {
    SUCCESS("success"),
    ERROR("error");

    private String returnValue;

    RulesReturnValues(String numVal) {
        this.returnValue = numVal;
    }

    @JsonValue
    public String getReturnValue() {
        return returnValue;
    }
}
